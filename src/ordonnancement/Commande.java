/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ordonnancement;

import java.util.ArrayList;

/**
 * Cette classe permet de définir une ensemble de tâches que devra réaliser le
 * processeur. Toutes les tâches se feront à la même vitesse.
 *
 * @author hubert
 */
public class Commande implements Comparable<Commande> {

    Date debut;
    Date fin;
    float vitesse;
    ArrayList<Tache> listeTaches;

    Commande(Date debut, Date fin, float vitesse) {
        this.debut = debut;
        this.fin = fin;
        this.vitesse = vitesse;
        listeTaches = new ArrayList<Tache>();
    }

    Commande(Date debut, Date fin, float vitesse, ArrayList<Tache> listeTaches) {
        this.debut = debut;
        this.fin = fin;
        this.vitesse = vitesse;
        this.listeTaches = listeTaches;
    }

    /**
     * Ajoute une tâche à la commande.
     *
     * @param t
     */
    void addTache(Tache t) {
        this.listeTaches.add(t);
    }

    /**
     * Deux commandes sont égales si leur dates de début sont égales.
     *
     * @param c
     * @return
     */
    public boolean equals(Commande c) {
        return debut.getAbsoluteTime() == c.debut.getAbsoluteTime();
    }

    /**
     * L'ordre des commandes est le même que celui des dates de début de la
     * classe Commande.
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(Commande t) {
        if (this.equals(t)) {
            return 0;
        }
        if (debut.getAbsoluteTime() < t.debut.getAbsoluteTime()) {
            return -1;
        }
        return 1;
    }
}
