/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ordonnancement;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Cette classe permet de donner la succession des tâches à fournir.
 *
 * @author hubert
 */
public class Processeur {

    private ArrayList<Commande> listeCommandes;

    Processeur() {
        listeCommandes = new ArrayList<Commande>();
    }

    /**
     * Cette fonction ajoute une commande à l'ordre d'eécution des tâches. Elle
     * tient compte du fait que certaines commandes peuvent s'intercecter avec
     * des commandes déjà présentes.
     *
     * @param commande
     */
    void add(Commande commande) {
        this.listeCommandes.add(commande);
        Collections.sort(listeCommandes);
        boolean appel = true;
        Commande courant = commande;

        while (appel) {
            int i = listeCommandes.indexOf(courant);
            if (i == listeCommandes.size() - 1) {
                return;
            }

            appel = false;
            if (courant.fin.getAbsoluteTime() > listeCommandes.get(i + 1).debut.getAbsoluteTime()) {

                if (courant.debut.getAbsoluteTime() != listeCommandes.get(i + 1).debut.getAbsoluteTime()) {
                    Commande seg1 = new Commande(courant.debut, listeCommandes.get(i + 1).debut, courant.vitesse, courant.listeTaches);
                    listeCommandes.add(seg1);

                    if (listeCommandes.get(i + 1).fin.getAbsoluteTime() != courant.fin.getAbsoluteTime()) {
                        Commande seg2 = new Commande(listeCommandes.get(i + 1).fin, courant.fin, courant.vitesse, courant.listeTaches);
                        listeCommandes.add(seg2);
                        appel = true;
                        listeCommandes.remove(courant);
                        Collections.sort(listeCommandes);
                        courant = seg2;
                    } else {
                        listeCommandes.remove(courant);
                        Collections.sort(listeCommandes);
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        String res = "";
        for (Commande c : listeCommandes) {
            res += "debut : " + c.debut + ", fin : " + c.fin + ", vitesse : " + c.vitesse + "\n";
            res += "" + c.listeTaches + "\n";
        }
        return res;
    }
}