/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ordonnancement;

/**
 * Cette classe abstraite modélise une date. Elle permet notamment de connaire
 * de qui elle est la date de début ou de fin
 *
 * @author hubert
 */
abstract class Date implements Comparable<Date> {

    private int relativeTime;
    private int absoluteTime;
    protected Tache t;

    protected Date(int time, Tache t) {
        this.relativeTime = time;
        this.absoluteTime = time;
        this.t = t;
    }

    @Override
    public String toString() {
        return "" + absoluteTime;
    }

    /**
     * Cette fonction permet de connaire la date associée à cette date par la
     * tâche associée.
     *
     * @return
     */
    abstract Date getPair();

    int getAbsoluteTime() {
        return this.absoluteTime;
    }

    int getRelativeTime() {
        return relativeTime;
    }

    void setAbsoluteTime(int time) {
        this.absoluteTime = time;
    }

    /**
     * Deux dates sont égales si elle pointe vers la même date.
     *
     * @param t
     * @return
     */
    public boolean equals(Date t) {
        return absoluteTime == t.absoluteTime;
    }

    /**
     * Permet de comarer deux dates.
     *
     * @param t
     * @return renvoie 0 si les deux dates sont égales au sens de equals, 1 si
     * la date t est postérieures, -1 sinon.
     */
    @Override
    public int compareTo(Date t) {
        if (this.equals(t)) {
            return 0;
        }
        if (this.absoluteTime < t.absoluteTime) {
            return -1;
        }
        return 1;
    }

    void setRelativeTime(int t) {
        this.relativeTime = t;
    }
    
    /**
     * renvoie la tâche associée à la date.
     * @return 
     */

    Tache getTache() {
        return t;
    }
}
