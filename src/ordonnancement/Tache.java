/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ordonnancement;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Une tâche comporte une date de début et de fin, un vitesse, un temps minimal
 * et une liste indiquant les moment pendant laquelle la tâche est préemptée.
 *
 * @author hubert
 */
public class Tache {

    private static int compteur;

    private static class Vide implements Comparable<Vide> {

        Date debut;
        Date fin;

        public Vide(Date a, Date b) {
            debut = a;
            fin = b;
        }

        @Override
        public int compareTo(Vide t) {
            return debut.compareTo(t.debut);
        }

        @Override
        public String toString() {
            String res = "[" + debut + ", " + fin + "]";
            return res;
        }
    }
    private Activation activation;
    private Echeance echeance;
    private int temps;
    public float vitesseProc;
    private int id;
    private ArrayList<Vide> listeVides;

    public Tache(int act, int ech, int temps) {
        if (ech < act) {
            throw new UnsupportedOperationException("l'heure d'activation doit être supérieure à l'échéance.");
        }
        if (temps < 0) {
            throw new UnsupportedOperationException("le temps doit etre supérieur à 0.");
        }
        if (temps > ech - act) {
            throw new UnsupportedOperationException("la durée de réalisation est supérieure au temps alloué à la tache.");
        }
        this.id = compteur++;
        this.temps = temps;
        activation = new Activation(act, this);
        echeance = new Echeance(ech, this);
        vitesseProc = 0;
        listeVides = new ArrayList<Vide>();
    }

    @Override
    public String toString() {
        String vide = "";
        if (!listeVides.isEmpty()) {
            vide = " - " + listeVides.toString();
        }
        return "id n° " + this.id + ", [[" + activation + "," + echeance + "]" + vide + ", " + temps + "]";
    }

    /**
     * Indique si la tâche est entre deux dates
     *
     * @param a date de début
     * @param b date de fin
     * @return
     */
    boolean isIn(int a, int b) {
        return (activation.getRelativeTime() >= a && echeance.getRelativeTime() <= b);
    }

    /**
     * Indique si la tâche est entre deux dates
     *
     * @param a date de début
     * @param b date de fin
     * @return
     */
    boolean isIn(Date a, Date b) {
        return (activation.getRelativeTime() >= a.getRelativeTime() && echeance.getRelativeTime() <= b.getRelativeTime());
    }

    int getTemps() {
        return temps;
    }

    Echeance getEcheance() {
        return echeance;
    }

    Activation getActivation() {
        return activation;
    }

    /**
     * Ajout une période pendant laquelle la tâche ne peut s'éxécuter.
     * @param debut
     * @param fin 
     */
    void addVide(Date debut, Date fin) {

        if (debut.compareTo(activation) <= 0 && echeance.compareTo(fin) <= 0) {
            throw new UnsupportedOperationException("La tache est complètement incluse dans le vide.");
        }

        if (activation.compareTo(debut) < 0 && fin.compareTo(echeance) < 0) {
            Vide vide = new Vide(debut, fin);
            listeVides.add(vide);
            Collections.sort(listeVides);

            int i = listeVides.indexOf(vide);
            boolean changement = true;
            while (changement) {
                changement = false;
                if (i != listeVides.size() - 1
                        && vide.fin.getAbsoluteTime() > listeVides.get(i + 1).debut.getAbsoluteTime()) {
                    Vide suivant = listeVides.get(i + 1);
                    if (suivant.fin.getAbsoluteTime() > vide.fin.getAbsoluteTime()) {
                        vide.fin = suivant.fin;
                    }
                    listeVides.remove(i + 1);
                    changement = true;
                }
                if (i != 0
                        && vide.debut.getAbsoluteTime() < listeVides.get(i - 1).fin.getAbsoluteTime()) {
                    Vide precedent = listeVides.get(i - 1);
                    if (precedent.fin.getAbsoluteTime() > vide.fin.getAbsoluteTime()) {
                        vide.fin = precedent.fin;
                    }
                    vide.debut = precedent.debut;
                    listeVides.remove(i - 1);
                    i--;
                    changement = true;
                }
            }
            return;
        }

        if (debut.getAbsoluteTime() <= activation.getAbsoluteTime()
                && activation.getAbsoluteTime() <= fin.getAbsoluteTime()) {
            System.out.print("décalage activation de " + this);
            activation.setAbsoluteTime(fin.getAbsoluteTime());
            deboutage();
            System.out.println(" à " + this);
            return;
        }
        if (debut.getAbsoluteTime() <= echeance.getAbsoluteTime()
                && echeance.getAbsoluteTime() <= fin.getAbsoluteTime()) {
            System.out.print("décalage echéance de " + this);
            echeance.setAbsoluteTime(debut.getAbsoluteTime());
            deboutage();
            System.out.println(" à " + this);
            return;
        }
    }

    /**
     * Suppression éventuelle des extremité de la tache
     */
    private void deboutage() {
        if (listeVides.isEmpty()) {
            return;
        }
        if (activation.getAbsoluteTime() >= listeVides.get(0).debut.getAbsoluteTime()) {
            activation.setAbsoluteTime(listeVides.get(0).fin.getAbsoluteTime());
            listeVides.remove(0);
        }
        if (echeance.getAbsoluteTime() <= listeVides.get(listeVides.size() - 1).fin.getAbsoluteTime()) {
            echeance.setAbsoluteTime(listeVides.get(listeVides.size() - 1).debut.getAbsoluteTime());
            listeVides.remove(listeVides.size() - 1);
        }
    }
}
