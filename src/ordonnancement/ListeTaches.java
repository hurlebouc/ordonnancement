/* branche Master
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ordonnancement;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Cette classe recueil l'ensemble des tâches à effectuer.
 *
 * @author hubert
 */
public class ListeTaches extends ArrayList<Tache> {

    private ArrayList<Activation> listeActivations;
    private ArrayList<Echeance> listeEcheances;

    public ListeTaches() {
        super();
        listeActivations = new ArrayList<Activation>();
        listeEcheances = new ArrayList<Echeance>();
    }

    /**
     * Ajoute une tâche à la liste.
     *
     * @param t
     * @return
     */
    @Override
    public boolean add(Tache t) {
        listeActivations.add(t.getActivation());
        listeEcheances.add(t.getEcheance());
        Collections.sort(listeActivations);
        Collections.sort(listeEcheances);
        return super.add(t);
    }

    /**
     * Suppression d'une tâche de la liste.
     *
     * @param i index de la tâche à supprimer
     * @return tâche supprimée de la liste
     */
    @Override
    public Tache remove(int i) {
        Tache t = this.get(i);
        listeActivations.remove(t.getActivation());
        listeEcheances.remove(t.getEcheance());
        return super.remove(i);
    }

    /**
     * Suppression de la tâche de la liste.
     *
     * @param tache tâche à supprimer
     * @return
     */
    @Override
    public boolean remove(Object tache) {
        Tache t = (Tache) tache;
        listeActivations.remove(t.getActivation());
        listeEcheances.remove(t.getEcheance());
        return super.remove(t);
    }

    @Override
    public String toString() {
        if (listeActivations.isEmpty()) {
            return "vide";
        }
        final String plein = "-";
        final String vide = " ";
        final String duree = "*";
        String res = super.toString();
        res = res + "\nTotal \t";
        int min = this.minTemps().getRelativeTime();
        int max = this.maxTemps().getRelativeTime();
        for (int i = 0; i < max - min; i++) {
            res = res + plein;
        }
        res += "\n";
        for (int i = 0; i < this.size(); i++) {
            res += i + "\t";
            Tache t = this.get(i);
            int debut = t.getActivation().getRelativeTime();
            int fin = t.getEcheance().getRelativeTime();
            int temps = t.getTemps();
            for (int j = 0; j < max - min; j++) {
                if (j < debut - min || j >= fin - min) {
                    res += vide;
                } else {
                    if (temps != 0) {
                        res += duree;
                        temps--;
                    } else {
                        res += plein;
                    }
                }
            }
            res += "\n";
        }

        return res;
    }

    Date maxTemps() {
        return Collections.max(listeEcheances);
    }

    Date minTemps() {
        return Collections.min(listeActivations);
    }

    /**
     * Cette fonction donne la valeur de l'intensité du le segment [a,b]
     *
     * @param a date d'activation
     * @param b date d'échéance
     * @return
     */
    private float intensite(int a, int b) {
        if (a == b) {
            throw new UnsupportedOperationException("on ne peut calculer l'intensité d'un segment d'intérieur vide");
        }
        if (a > b) {
            throw new UnsupportedOperationException("la borne inf doit être inférieure à la sup");
        }
        float somme = 0;
        for (Tache tache : this) {
            if (tache.isIn(a, b)) {
                somme += tache.getTemps();
            }
        }
        return somme / (b - a);
    }

    /**
     * Cette fonction compresse l'intervalle de temps
     *
     * @param indexAct index de la date d'activation
     * @param indexEch index de la date d'échéance
     */
    private void compresse(int indexAct, int indexEch, Processeur proc) {
        Activation a = listeActivations.get(indexAct);
        Echeance b = listeEcheances.get(indexEch);
        float vitesse = this.intensite(a.getRelativeTime(), b.getRelativeTime());
        Commande commande = new Commande(a, b, vitesse);

        System.out.println("compression de " + a + " à " + b + ", vitesse : " + vitesse);


        int ecart = b.getRelativeTime() - a.getRelativeTime();

        int n = indexAct;
        int l = listeActivations.size();
        boolean depassement = false;
        while (n < l && !depassement) {
            Activation act = listeActivations.get(n);
            if (act.getRelativeTime() > b.getRelativeTime()) {
                depassement = true;
            } else {
                Tache t = act.getTache();
                if (t.isIn(a, b)) {
                    t.vitesseProc = vitesse;
                    System.out.println("" + t);
                    commande.addTache(t);
                    this.remove(t);
                    l--;
                } else {
                    n++;
                }
            }
        }
        proc.add(commande);
        l = listeActivations.size();


        for (int i = 0; i < l; i++) {
            Tache t = this.get(i);

            Activation act = t.getActivation();
            int t_ini_act = act.getRelativeTime();
            int ecart_act = t_ini_act - a.getRelativeTime();
            if (a.getRelativeTime() <= t_ini_act) {
                act.setRelativeTime(t_ini_act - Math.min(ecart, ecart_act));
            }

            Echeance ech = t.getEcheance();
            int t_ini_ech = ech.getRelativeTime();
            int ecart_ech = t_ini_ech - a.getRelativeTime();
            if (a.getRelativeTime() <= t_ini_ech) {
                ech.setRelativeTime(t_ini_ech - Math.min(ecart, ecart_ech));
            }

            t.addVide(a, b);

        }
    }

    /**
     * Donne les index les dates d'activation et d'échéance qui maximise la
     * fonction d'intensité. On ajoute la condition qu'il soit le plus large
     * possible au sens des index.
     *
     * @return tableau de taille 2 donnant les index des dates
     */
    private int[] getIntervalleCritique() {
        int l = listeActivations.size();
        int[] res = {0, 0};
        float wmax = 0;
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < l; j++) {
                Activation act = listeActivations.get(i);
                Echeance ech = listeEcheances.get(j);
                if (act.getRelativeTime() < ech.getRelativeTime()) {
                    float w = this.intensite(act.getRelativeTime(), ech.getRelativeTime());
                    if (w > wmax) {
                        res[0] = i;
                        res[1] = j;
                        wmax = w;
                    }
                }
            }
        }
        while (res[0] > 0
                && listeActivations.get(res[0] - 1).getRelativeTime() == listeActivations.get(res[0]).getRelativeTime()) {
            res[0]--;
        }
        while (res[1] + 1 < l
                && listeEcheances.get(res[1] + 1).getRelativeTime() == listeEcheances.get(res[1]).getRelativeTime()) {
            res[1]++;
        }
        return res;
    }

    /**
     * Effectue l'algorithme de Yao.
     */
    public void yao() {
        Processeur proc = new Processeur();
        while (this.listeActivations.size() > 0) {
            int[] intervalleCritique = this.getIntervalleCritique();
            this.compresse(intervalleCritique[0], intervalleCritique[1], proc);
            //System.out.println("taches : " + this);
        }
        System.out.println(proc);
    }
}
