/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ordonnancement;

/**
 *
 * @author hubert
 */
class Echeance extends Date {

    Echeance(int valeur, Tache t) {
        super(valeur, t);
    }

    @Override
    Date getPair() {
        return t.getActivation();
    }
}
