/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package execution;

import ordonnancement.ListeTaches;
import ordonnancement.Tache;

/**
 *
 * @author hubert
 */
public class Ordonnancement {
    
    public final static int TAILLE_ECHELLE_TEMPS = 10;
    public final static int NBR_TACHES = 5;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ListeTaches liste = new ListeTaches();
        
//        for (int i = 0; i < NBR_TACHES; i++) {
//            int n = (int) (TAILLE_ECHELLE_TEMPS * Math.random());
//            int m = (int) (TAILLE_ECHELLE_TEMPS * Math.random());
//            while (m == n) {
//                m = (int) (TAILLE_ECHELLE_TEMPS * Math.random());
//            }
//            int act = Math.min(n, m);
//            int ech = Math.max(n, m);
//            int temps = (int) ((ech-act)*Math.random() + 1);
//            liste.add(new Tache(act, ech, temps));
//        }
        
//        liste.add(new Tache(0,5,1));
//        liste.add(new Tache(3,8,3));
//        liste.add(new Tache(2,4,2));
//        liste.add(new Tache(8,10,1));
//        liste.add(new Tache(12,14,1));
//        liste.add(new Tache(9,14,5));
        
//        liste.add(new Tache(0,2,1));
//        liste.add(new Tache(1,7,3));
//        liste.add(new Tache(1,4,3));
//        liste.add(new Tache(6,11,2));
//        liste.add(new Tache(13,15,1));
        
        liste.add(new Tache(0,9,4));
        liste.add(new Tache(2,6,4));
        
        System.out.println("Initialisation");
        System.out.println("==============");
        System.out.println("Tâches : " + liste);
        //System.out.println("activations : " + liste.listeActivations);
        //System.out.println("echéance : " + liste.listeEcheances);
        
        liste.yao();
        
//        for (int i = 0; i < liste.size(); i++) {
//            System.out.println("" + i + " " + liste.get(i) + liste.get(i).vitesseProc);
//        }
        
    }
}
